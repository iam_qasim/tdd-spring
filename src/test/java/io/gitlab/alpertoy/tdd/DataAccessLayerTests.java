package io.gitlab.alpertoy.tdd;

import io.gitlab.alpertoy.tdd.entities.Account;
import io.gitlab.alpertoy.tdd.repositories.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class DataAccessLayerTests {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    void testFindById() {
        Optional<Account> account = accountRepository.findById(1L);
        assertTrue(account.isPresent());
        assertEquals("Alper", account.orElseThrow().getUsername());
    }

    @Test
    void testFindByUsername() {
        Optional<Account> account = accountRepository.findByUsername("Alper");
        assertTrue(account.isPresent());
        assertEquals("Alper", account.orElseThrow().getUsername());
        assertEquals("1000.00", account.orElseThrow().getBalance().toPlainString());
    }

    @Test
    void testFindByUsernameThrowException() {
        Optional<Account> account = accountRepository.findByUsername("Mark");
        assertThrows(NoSuchElementException.class, account::orElseThrow);
        assertFalse(account.isPresent());
    }

    @Test
    void testFindAll() {
        List<Account> account = accountRepository.findAll();
        assertFalse(account.isEmpty());
        assertEquals(2, account.size());
    }

    @Test
    void testSave() {
        // Given
        Account accountLilly = new Account(null, "Lilly", new BigDecimal("3000"));

        // When
        Account account = accountRepository.save(accountLilly);

        // Then
        assertEquals("Lilly", account.getUsername());
        assertEquals("3000", account.getBalance().toPlainString());
    }

    @Test
    void testUpdate() {
        // Given
        Account accountLilly = new Account(null, "Lilly", new BigDecimal("3000"));

        // When
        Account account = accountRepository.save(accountLilly);

        // Then
        assertEquals("Lilly", account.getUsername());
        assertEquals("3000", account.getBalance().toPlainString());

        // When
        account.setBalance(new BigDecimal("3800"));
        Account actualAccount = accountRepository.save(account);

        // Then
        assertEquals("Lilly", actualAccount.getUsername());
        assertEquals("3800", actualAccount.getBalance().toPlainString());

    }

    @Test
    void testDelete() {
        Account account = accountRepository.findById(2L).orElseThrow();
        assertEquals("John", account.getUsername());

        accountRepository.delete(account);

        try {
            accountRepository.findById(2L).orElseThrow();
        }
        catch (NoSuchElementException e) {
            e.getMessage();
        }
        assertEquals(1, accountRepository.findAll().size());
    }
}
