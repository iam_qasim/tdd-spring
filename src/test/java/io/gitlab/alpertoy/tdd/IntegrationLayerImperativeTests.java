package io.gitlab.alpertoy.tdd;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitlab.alpertoy.tdd.dtos.TransactionDTO;
import io.gitlab.alpertoy.tdd.entities.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationLayerImperativeTests {

    @Autowired
    private TestRestTemplate client;

    private ObjectMapper objectMapper;

    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    private String createURI(String uri) {
        return "http://localhost:" + port + uri;
    }

    @Test
    @Order(1)
    void testTransfer() throws JsonProcessingException {
        TransactionDTO dto = new TransactionDTO();
        dto.setAmount(new BigDecimal("100"));
        dto.setToAccountId(2L);
        dto.setFromAccountId(1L);
        dto.setBankId(1L);

        ResponseEntity<String> response = client.
                postForEntity(createURI("/api/accounts/transfer"), dto, String.class);
        System.out.println(port);
        String json = response.getBody();
        System.out.println(json);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertNotNull(json);
        assertTrue(json.contains("Money has been transferred successfully!"));
        assertTrue(json.contains("{\"fromAccountId\":1,\"toAccountId\":2,\"amount\":100,\"bankId\":1}"));

        JsonNode jsonNode = objectMapper.readTree(json);
        assertEquals("Money has been transferred successfully!", jsonNode.path("message").asText());
        assertEquals(LocalDate.now().toString(), jsonNode.path("date").asText());
        assertEquals("100", jsonNode.path("transaction").path("amount").asText());
        assertEquals(1L, jsonNode.path("transaction").path("fromAccountId").asLong());

        Map<String, Object> response2 = new HashMap<>();
        response2.put("date", LocalDate.now().toString());
        response2.put("status", "OK");
        response2.put("message", "Money has been transferred successfully!");
        response2.put("transaction", dto);

        assertEquals(objectMapper.writeValueAsString(response2), json);

    }

    @Test
    @Order(2)
    void testFindAccount() {
        ResponseEntity<Account> response = client.getForEntity(createURI("/api/accounts/1"), Account.class);
        Account account = response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());

        assertNotNull(account);
        assertEquals(1L, account.getId());
        assertEquals("Alper", account.getUsername());
        assertEquals("1000.00", account.getBalance().toPlainString());
        assertEquals(new Account(1L, "Alper", new BigDecimal("1000.00")), account);
    }

    @Test
    @Order(3)
    void testFindAllAccounts() throws JsonProcessingException {
        ResponseEntity<Account[]> response = client.getForEntity(createURI("/api/accounts"), Account[].class);
        List<Account> accounts = Arrays.asList(response.getBody());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());

        assertEquals(2, accounts.size());
        assertEquals(1L, accounts.get(0).getId());
        assertEquals("Alper", accounts.get(0).getUsername());
        assertEquals("1000.00", accounts.get(0).getBalance().toPlainString());
        assertEquals(2L, accounts.get(1).getId());
        assertEquals("John", accounts.get(1).getUsername());
        assertEquals("2000.00", accounts.get(1).getBalance().toPlainString());

        JsonNode json = objectMapper.readTree(objectMapper.writeValueAsString(accounts));
        assertEquals(1L, json.get(0).path("id").asLong());
        assertEquals("Alper", json.get(0).path("username").asText());
        assertEquals("1000.0", json.get(0).path("balance").asText());
        assertEquals(2L, json.get(1).path("id").asLong());
        assertEquals("John", json.get(1).path("username").asText());
        assertEquals("2000.0", json.get(1).path("balance").asText());
    }

    @Test
    @Order(4)
    void testSaveAccount() {
        Account account = new Account(null, "Mark", new BigDecimal("3800"));

        ResponseEntity<Account> response = client.postForEntity(createURI("/api/accounts"), account, Account.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        Account createdAccount = response.getBody();
        assertNotNull(createdAccount);
        assertEquals(3L, createdAccount.getId());
        assertEquals("Mark", createdAccount.getUsername());
        assertEquals("3800", createdAccount.getBalance().toPlainString());
    }

    @Test
    @Order(5)
    void testDeleteAccount() {

        ResponseEntity<Account[]> response = client.getForEntity(createURI("/api/accounts"), Account[].class);
        List<Account> accounts = Arrays.asList(response.getBody());
        assertEquals(3, accounts.size());

        //client.delete(createURI("/api/accounts/3"));
        Map<String, Long> pathVariables = new HashMap<>();
        pathVariables.put("id", 3L);
        ResponseEntity<Void> exchange = client.exchange(createURI("/api/accounts/{id}"), HttpMethod.DELETE, null, Void.class,
                pathVariables);

        assertEquals(HttpStatus.NO_CONTENT, exchange.getStatusCode());
        assertFalse(exchange.hasBody());

        response = client.getForEntity(createURI("/api/accounts"), Account[].class);
        accounts = Arrays.asList(response.getBody());
        assertEquals(2, accounts.size());

        ResponseEntity<Account> responseDetail = client.getForEntity(createURI("/api/accounts/3"), Account.class);
        assertEquals(HttpStatus.NOT_FOUND, responseDetail.getStatusCode());
        assertFalse(responseDetail.hasBody());
    }
}
