package io.gitlab.alpertoy.tdd;

import io.gitlab.alpertoy.tdd.controllers.AccountController;
import io.gitlab.alpertoy.tdd.dtos.TransactionDTO;
import io.gitlab.alpertoy.tdd.entities.Account;
import io.gitlab.alpertoy.tdd.entities.Bank;
import io.gitlab.alpertoy.tdd.services.AccountService;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

@WebMvcTest(AccountController.class)
public class ServiceAndPresentationLayerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    //Creating objects for tests because we are in scope of " test " in h2 with no data & connection
    public static Optional<Account> createAccount01() {
        return Optional.of(new Account(1L, "Alper", new BigDecimal("1000")));
    }

    public static Optional<Account> createAccount02() {
        return Optional.of(new Account(2L, "John", new BigDecimal("2000")));
    }

    public static Optional<Bank> createBank() {
        return Optional.of(new Bank(1L, "The Spring Bank", 0));
    }

    @Test
    void testFindAccount() throws Exception {
        // Given
        when(accountService.findById(1L)).thenReturn(createAccount01().orElseThrow());

        // When
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/1").contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("Alper"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.balance").value("1000"));

        verify(accountService).findById(1L);
    }

    @Test
    void testTransferMoney() throws Exception, JsonProcessingException {

        // Given
        TransactionDTO dto = new TransactionDTO();
        dto.setFromAccountId(1L);
        dto.setToAccountId(2L);
        dto.setAmount(new BigDecimal("100"));
        dto.setBankId(1L);

        System.out.println(objectMapper.writeValueAsString(dto));

        Map<String, Object> response = new HashMap<>();
        response.put("date", LocalDate.now().toString());
        response.put("status", "OK");
        response.put("message", "Money has been transferred successfully!");
        response.put("transaction", dto);

        System.out.println(objectMapper.writeValueAsString(response));

        // When
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dto)))

                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value(LocalDate.now().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Money has been transferred successfully!"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.transaction.fromAccountId").value(dto.getFromAccountId()))
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(response)));

    }

    @Test
    void testFindAllAccounts() throws Exception {
        // Given
        List<Account> accounts = Arrays.asList(createAccount01().orElseThrow(),
                createAccount02().orElseThrow()
        );
        when(accountService.findAll()).thenReturn(accounts);

        // When
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts").contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value("Alper"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].username").value("John"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].balance").value("1000"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].balance").value("2000"))
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(accounts)));

        verify(accountService).findAll();
    }

    @Test
    void testSaveAccount() throws Exception {
        // Given
        Account account = new Account(null, "Mark", new BigDecimal("3000"));
        when(accountService.save(any())).then(call ->{
            Account a = call.getArgument(0);
            a.setId(3L);
            return a;
        });

        // when
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(account)))
                // Then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username", is("Mark")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.balance", is(3000)));
        verify(accountService).save(any());

    }
}
