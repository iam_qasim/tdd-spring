package io.gitlab.alpertoy.tdd.services;

import io.gitlab.alpertoy.tdd.entities.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    List<Account> findAll();

    Account findById(Long id);

    Account save(Account account);

    void deleteById(Long id);

    int getTotalTransfers(Long bankId);

    BigDecimal getBalance(Long accountId);

    void transfer(Long fromAccountNum, Long toAccountNum, BigDecimal amount, Long bankId);
}
