package io.gitlab.alpertoy.tdd.repositories;

import io.gitlab.alpertoy.tdd.entities.Account;
import org.springframework.data.jpa.repository.*;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("select a from Account a where a.username=?1")
    Optional<Account> findByUsername(String username);
}