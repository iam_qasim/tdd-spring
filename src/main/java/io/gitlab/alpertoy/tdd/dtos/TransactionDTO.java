package io.gitlab.alpertoy.tdd.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionDTO {

    private Long fromAccountId;
    private Long toAccountId;
    private BigDecimal amount;
    private Long bankId;

}
